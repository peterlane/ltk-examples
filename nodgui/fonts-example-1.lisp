(require 'asdf)
(require 'nodgui)
(use-package :nodgui)

(with-nodgui ()
          (let ((label-1 (make-instance 'label :text "default font"))
                (label-2 (make-instance 'label :text "font: helvetica 12 bold"
                                        :font "Helvetica 12 bold"))
                (label-3 (make-instance 'label :text "font: courier 8"
                                        :font "Courier 8")))

            (grid label-1 0 0)
            (grid label-2 1 0)
            (grid label-3 2 0)))
