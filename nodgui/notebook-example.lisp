(require 'asdf)
(require 'nodgui)
(use-package :nodgui)

(with-nodgui ()
          (let ((notebook (make-instance 'notebook)))
            (grid notebook 0 0 :sticky "news")
            (grid-columnconfigure *tk* 0 :weight 1)
            (grid-rowconfigure *tk* 0 :weight 1)

            ;; add three panes to the notebook
            (dolist (pane '("Red" "Green" "Blue"))
              (let ((frame (make-instance 'frame :master notebook)))
                (grid (make-instance 'button
                                     :master frame
                                     :text (format nil "Pane ~a" pane)
                                     :command (lambda ()
                                                (format t "Clicked on button in pane ~a~%" pane)))
                      0 0
                      :padx 10
                      :pady 10)
                (notebook-add notebook frame :text pane)))))
