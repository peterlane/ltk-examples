(require 'asdf)
(require 'nodgui)
(use-package :nodgui)

(with-nodgui ()
          (let ((colour (choose-color :title "Select text colour"
                                      :initial-color :red)))
            
            (when (string= colour "") 
              (setf colour :blue))

            (grid (make-instance 'label
                                 :text (format nil "In colour ~a" colour)
                                 :foreground colour)
                  0 0)))
