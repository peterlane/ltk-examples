(require 'asdf)
(require 'nodgui)
(use-package :nodgui)

(with-nodgui ()
          (wm-title *tk* "spinbox-example.lisp")
          (let* ((days (make-instance 'spinbox :from 1 :to 31))
                 (months (make-instance 'spinbox 
                                        :state :readonly
                                        :values '("January" "February" "March"
                                                  "April" "May" "June" "July"
                                                  "August" "September" "October"
                                                  "November" "December")))
                 (show (make-instance 'button :text "Show date"
                                      :command #'(lambda ()
                                                   (format t "~a ~a~%"
                                                           (text days)
                                                           (text months))))))
            (grid (make-instance 'label :text "Day:") 0 0)
            (grid days 0 1 :sticky "we")
            (grid (make-instance 'label :text "Month:") 1 0)
            (grid months 1 1 :sticky "we")
            (grid show 2 0 :columnspan 2)))
