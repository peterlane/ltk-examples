(require 'asdf)
(require 'nodgui)
(use-package :nodgui)

(with-nodgui ()
          (wm-title *tk* "scale-example.lisp")
          (let* ((label (make-instance 'label :text "Scale value: 20"))
                 (scale (make-instance 'scale :orientation :horizontal :length 200
                                       :from 1 :to 100
                                       :command #'(lambda (val) 
                                                    (setf (text label) 
                                                          (format nil "Scale value: ~a" val))))))
            (grid label 0 0)
            (grid scale 1 0 :sticky "we")

            (setf (value scale) 20)))


