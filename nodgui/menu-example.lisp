(require 'asdf)
(require 'nodgui)
(use-package :nodgui)

(with-nodgui ()
          (wm-title *tk* "Menu Example")
          (let* ((menubar (make-instance 'menubar)) 
                 ; add some menus to menubar
                 (file-menu (make-instance 'menu :master menubar :text "File"))
                 (example-menu (make-instance 'menu :master menubar :text "Example"))
                 )
           ; add command items to 'file'
           (make-instance 'menubutton :master file-menu :text "New"
                          :command (lambda () (format t "You clicked 'New'~%")))
           (configure
             (make-instance 'menubutton :master file-menu :text "Save")
             :state :disabled)
           (add-separator file-menu)
           (make-instance 'menubutton :master file-menu :text "Quit" :underline 0
                          :command (lambda () (uiop:quit)))
           ; -- build 'example' menu
           (let* ((check-menu (make-instance 'menucheckbutton :master example-menu 
                                             :text "Select"))
                  (colours (make-instance 'menu :master example-menu 
                                          :text "Colours"))
                  (red-button (make-instance 'menuradiobutton :master colours 
                                             :text "Red" :name :red :group :colours))
                  (green-button (make-instance 'menuradiobutton :master colours 
                                               :text "Green" :name :green :group :colours))
                  (blue-button (make-instance 'menuradiobutton :master colours 
                                              :text "Blue" :name :blue :group :colours)))
             (add-separator example-menu)
             (make-instance 'menubutton :master example-menu :text "Show"
                            :command (lambda () 
                                       (format t "Values are: ~%Select: ~a~%Colours: ~a~%" 
                                               (value check-menu)
                                               (value red-button)))))))

