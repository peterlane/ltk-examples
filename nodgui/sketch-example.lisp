(require 'asdf)
(require 'nodgui)
(use-package :nodgui)

(with-nodgui ()
          (wm-title *tk* "sketch-example.lisp")
          (let* ((last-x 0)
                 (last-y 0)
                 (colour :blue)
                 (canvas (make-instance 'canvas :width 500 :height 400 :background :gray75))
                 (add-line #'(lambda (x y) 
                               (configure (make-line canvas (list last-x last-y x y))
                                          :fill colour 
                                          :width 5
                                          :tag "currentline")
                               (setf last-x x
                                     last-y y))))

            (grid canvas 0 0 :sticky "news")
            (grid-columnconfigure *tk* 0 :weight 1)
            (grid-rowconfigure *tk* 0 :weight 1)

            (bind canvas "<1>" #'(lambda (evt) (setf last-x (event-x evt)
                                                     last-y (event-y evt))))
            (bind canvas "<B1-Motion>" 
                  #'(lambda (evt) (funcall add-line (event-x evt) (event-y evt))))
            (bind canvas "<B1-ButtonRelease>"
                  #'(lambda (evt) (tag-configure canvas "currentline" :width 1)))
            
            ;; add three rectangles, and option to change colour
            (let ((r (make-rectangle canvas 10 10 30 30)))
              (configure r :fill :red)
              (bind r "<1>" #'(lambda (evt) (setf colour :red))))
            (let ((r (make-rectangle canvas 10 35 30 55)))
              (configure r :fill :blue)
              (bind r "<1>" #'(lambda (evt) (setf colour :blue))))
            (let ((r (make-rectangle canvas 10 60 30 80)))
              (configure r :fill :black)
              (bind r "<1>" #'(lambda (evt) (setf colour :black))))
            ))


