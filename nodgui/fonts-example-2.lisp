(require 'asdf)
(require 'nodgui)
(use-package :nodgui)

(with-nodgui ()
          (let ((fonts (sort (font-families) #'string<)))
            (format t "There are ~d font families, e.g. ~&\"~a\" ~&\"~a\" ~&\"~a\"~&" 
                    (length fonts) (first fonts)
                    (second fonts) (nth 200 fonts))

            (font-create "special-font" :family (second fonts))
            (format t "Metrics for our font are: ~a~&" (font-metrics "special-font"))
            (font-configure "special-font" :size 24)
            (format t "-- after setting size 24: ~a~&" (font-metrics "special-font"))

            (grid (make-instance 'label 
                                 :text (format nil "font: ~a 24pt" (second fonts))
                                 :font "special-font")
                  0 0)))
