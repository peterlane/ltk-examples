(require 'asdf)
(require 'nodgui)
(use-package :nodgui)
(setf *debug-tk* t)

(with-nodgui ()
          (let ((file-to-open (get-open-file :file-types '(("Images" "*.gif") ("Lisp" "*.lisp"))
                                             :title "Open some file"))
                (files-to-save (get-save-file ))
                (directory-choice (choose-directory)))

            (format t "You chose ~&~a file to open~&~a filename for saving~&~a directory~&"
                    file-to-open files-to-save directory-choice)))
