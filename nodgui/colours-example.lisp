(require 'asdf)
(require 'nodgui)
(use-package :nodgui)

(with-nodgui ()
          (wm-title *tk* "colours-example.lisp")
          (let ((label-1 (make-instance 'label :text "default colour"))
                (label-2 (make-instance 'label :text "colour by name (:red)"
                                        :foreground :red))
                (label-3 (make-instance 'label :text "colour by RGB (#03FF2C/#FFFFFF)"
                                        :foreground "#03FF2C"
                                        :background "#FFFFFF")))

            (grid label-1 0 0)
            (grid label-2 1 0)
            (grid label-3 2 0)))
