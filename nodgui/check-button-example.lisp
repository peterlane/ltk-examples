(require 'asdf)
(require 'nodgui)
(use-package :nodgui)

(with-nodgui ()
          (wm-title *tk* "check-button-example.lisp")
          (let* ((button-1 (make-instance 'check-button :text "Simple text label"
                                         :command (lambda (value) (format t "button-1 now ~a~&"
                                                                           value))))
                 (image (make-image "tcllogo.gif"))
                 (button-2 (make-instance 'check-button :image image
                                          :command (lambda (value) (format t "button-2 ~a~&"
                                                                           value))))
                 (button-3 (make-instance 'check-button :image image :text "Tcl Logo"
                                          :command (lambda (value) (format t "button-3 ~a~&"
                                                                           value))))
                 (show (make-instance 'button :text "Show states"
                                      :command (lambda ()
                                                 (format t "Buttons ~a ~a ~a~&"
                                                         (value button-1)
                                                         (value button-2)
                                                         (value button-3))))))

            (setf (value button-1) t)
            (configure button-3 :compound :bottom)

            (grid button-1 0 0 :padx 5 :pady 5)
            (grid button-2 0 1 :padx 5 :pady 5)
            (grid button-3 0 3 :padx 5 :pady 5)
            (grid show 1 0 :columnspan 3)))
