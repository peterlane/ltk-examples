(require 'asdf)
(require 'nodgui)
(use-package :nodgui)

(with-nodgui ()
          (wm-title *tk* "window-example.lisp")
          (let* ((window-1 '())
                 (window-2 '())
                 (open-1 (make-instance 'button :text "Open 1"
                                        :command #'(lambda () 
                                                     (unless window-1
                                                       (setf window-1
                                                             (make-toplevel *tk*))
                                                       (wm-title window-1 "window 1")
                                                       (iconify window-1)))))
                 (open-2 (make-instance 'button :text "Open 2"
                                        :command #'(lambda ()
                                                     (unless window-2
                                                       (setf window-2
                                                             (make-instance 'toplevel
                                                                            :title "window 2 - unresizable"))
                                                       (resizable window-2 0 0)))))
                 (delete-1 (make-instance' button :text "Close 1"
                                           :command #'(lambda ()
                                                        (when window-1
                                                          (destroy window-1)
                                                          (setf window-1 nil)))))
                 (delete-2 (make-instance' button :text "Close 2"
                                           :command #'(lambda ()
                                                        (when window-2
                                                          (destroy window-2)
                                                          (setf window-2 nil)))))
                 )

            (on-close *tk* 
                      #'(lambda ()
                          (when (ask-yesno "Do you really want to close?" 
                                           :title "Closing program")
                            (uiop:quit))))

            (format t "Geometry of root at start: ~a~&" (geometry *tk*))

            (grid open-1 0 0)
            (grid open-2 0 1)
            (grid delete-1 1 0)
            (grid delete-2 1 1)

            (format t "Geometry of root when filled: ~a~&" (geometry *tk*))
            ))

