(require 'asdf)
(require 'ltk)
(use-package :ltk)

(with-ltk ()
          (wm-title *tk* "combobox-example.lisp")
          (let ((cb1 (make-instance 'combobox :values '(red green blue)))
                (cb2 (make-instance 'combobox :values '(red green blue) :state 'readonly))
                )

            (grid cb1 0 0 :pady 10)
            (grid cb2 1 0 :pady 10)

            (bind cb2 "<<ComboboxSelected>>"
                  (lambda (evt) (format t "cb2 is now ~a~%" (text cb2))))))
