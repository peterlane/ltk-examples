(require 'asdf)
(require 'ltk)
(use-package :ltk)

(with-ltk ()
          (let ((panes (make-instance 'paned-window)))
            (grid panes 0 0 :sticky "nsew")       
            (grid-columnconfigure *tk* 0 :weight 1)
            (grid-rowconfigure *tk* 0 :weight 1)

            (let ((frame (make-instance 'labelframe 
                                        :master panes
                                        :text "Horizontal separator")))
              (grid (make-instance 'label :master frame :text "Label 1")
                    0 0)
              (grid (make-instance 'separator :master frame) 
                    1 0 :sticky "we")
              (grid (make-instance 'label :master frame :text "Label 2")
                    2 0)
              (grid frame 0 0 :padx 5 :pady 5)
              (add-pane panes frame))

            (let ((frame (make-instance 'labelframe 
                                        :master panes
                                        :text "Vertical separator")))
              (grid (make-instance 'label :master frame :text "Label 1") 
                    0 0)
              (grid (make-instance 'separator :master frame :orientation :vertical) 
                    0 1 :sticky "ns")
              (grid (make-instance 'label :master frame :text "Label 2")
                    0 2)
              (grid frame 0 0 :padx 5 :pady 5)
              (add-pane panes frame))))
