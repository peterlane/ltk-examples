(require 'asdf)
(require 'ltk)
(use-package :ltk)

;; country databases
;; - the list of country codes (a subset anyway)
;; - parallel list of country names, same order as the country codes
;; - an assoc-list mapping country code to population

(defparameter *country-codes* '(ar au be br ca cn dk fi fr gr in it jp mx nl no
                                   es se ch))
(defparameter *country-names* '("Argentina" "Australia" "Belgium" "Brazil" "Canada" "China"
                                          "Denmark" "Finland" "France" "Greece" "India"
                                          "Italy" "Japan" "Mexico" "Netherlands" "Norway"
                                          "Spain" "Sweden" "Switzerland"))
(defparameter *populations* '((ar . 41000000) (au . 21179211) (be . 10584534) (br . 185971537)
                                              (ca . 33148682) (cn . 1323128240) (dk . 5457415) (fi . 5302000) 
                                              (fr . 64102140) (gr . 11147000) (in . 1131043000) (it . 59206382)
                                              (jp . 127718000) (mx . 106535000) (nl . 16402414) (no . 4738085)
                                              (es . 45116894) (se . 9174082) (ch . 7508700)))
(defparameter *gifts* '(("card" . "Greeting card") ("flowers" . "Flowers") ("nastygram" . "Nastygram")))

(defun get-gift-name (gift)
  (cdr (assoc (string-downcase (string gift)) *gifts*
              :test #'string=)))

(defun send-gift (index gift sent-label)
  "Sends gift with given index, and updates text in sent-label"
  (when (= 1 (length index))
    (let* ((idx (first index))
           (gift (get-gift-name gift))
           (country (nth idx *country-names*)))

      (setf (text sent-label) 
            (format nil "Sent ~a to leader of ~a" gift country)))))

(defun show-population (index status-label sent-label)
  "Updates status label with information for given country index"
  (format t "Index ~a~&" index)
  (when (= 1 (length index))
    (let* ((idx (first index))
           (code (nth idx *country-codes*))
           (name (nth idx *country-names*))
           (popn (cdr (assoc code *populations*))))
      (setf (text status-label)
            (format nil "The population of ~a (~a) is ~a" name code popn))
      (setf (text sent-label) ""))))

(with-ltk ()
          (wm-title *tk* "Listbox Example: Gift Sending")
          ; create the outer content frame and other widgets
          (let* ((content (make-instance 'frame))
                 (countries (make-instance 'scrolled-listbox :master content))
                 (send-label (make-instance 'label :master content :text "Send to country's leader:"))
                 (gift-1 (make-instance 'radio-button :master content 
                                        :text (get-gift-name "card")
                                        :value "card" :variable "gift"))
                 (gift-2 (make-instance 'radio-button :master content 
                                        :text (get-gift-name "flowers")
                                        :value "flowers" :variable "gift"))
                 (gift-3 (make-instance 'radio-button :master content 
                                        :text (get-gift-name "nastygram")
                                        :value "nastygram" :variable "gift"))
                 (sent-label (make-instance 'label :master content :text "" :anchor :center))
                 (status-label (make-instance 'label :master content :text "" :anchor "w"))
                 (send (make-instance 'button :master content :text "Send Gift"
                                      :command #'(lambda () 
                                                   (send-gift (listbox-get-selection countries) (value gift-1) sent-label))
                                      :default :active))
                 )
            ; grid the outer content frame
            (configure content :padding "5 5 12 0")
            (grid content 0 0 :sticky "nwes")
            (grid-columnconfigure *tk* 0 :weight 1)
            (grid-rowconfigure *tk* 0 :weight 1)
            (grid-columnconfigure content 0 :weight 1)
            (grid-rowconfigure content 5 :weight 1)
            ; grid the other widgets
            (listbox-append countries *country-names*)
            (grid countries 0 0 :rowspan 6 :sticky "nsew")
            (grid send-label 0 1 :padx 10 :pady 10)
            (grid gift-1 1 1 :sticky "w" :padx 20)
            (grid gift-2 2 1 :sticky "w" :padx 20)
            (grid gift-3 3 1 :sticky "w" :padx 20)
            (grid send 4 2 :sticky "e")
            (grid sent-label 5 1 :columnspan 2 :sticky "n" :padx 5 :pady 5)
            (grid status-label 6 0 :columnspan 2 :sticky "we")

            ; Set event bindings for when the selection in the listbox changes,
            ; when the user double clicks the list, and when they hit the Return key
            (bind (listbox countries) "<<ListboxSelect>>" #'(lambda (evt) (show-population (listbox-get-selection countries)
                                                                                           status-label sent-label)))
            (bind (listbox countries) "<Double-1>" #'(lambda (evt) (send-gift (listbox-get-selection countries) (value gift-1) sent-label)))
            (bind *tk* "<Return>" #'(lambda (evt) (send-gift (listbox-get-selection countries) (value gift-1) sent-label)))

            (setf (value gift-1) 'card)
            (listbox-select countries 0)
            (show-population (listbox-get-selection countries) status-label sent-label)
            ; alternate colours in listbox
            (dotimes (i (length *country-names*))
              (when (evenp i)
                (listbox-configure (listbox countries) i :background "#f0f0ff")))))

