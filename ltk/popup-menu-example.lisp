(require 'asdf)
(require 'ltk)
(use-package :ltk)

(with-ltk ()
          (let ((menu (make-instance 'menu)))
            (dolist (item '("One" "Two" "Three"))
              (make-instance 'menubutton 
                             :master menu
                             :text item
                             :command (lambda () 
                                        (format t "You clicked ~a~%" item))))
            (bind *tk*
                  "<3>" 
                  (lambda (evt) 
                    (popup menu (event-root-x evt) (event-root-y evt))))))
