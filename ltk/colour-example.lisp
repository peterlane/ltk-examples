(require 'asdf)
(require 'ltk)
(use-package :ltk)

(with-ltk ()
          (let ((colour (choose-color :title "Select text colour"
                                      :initialcolor :red)))
            
            (when (string= colour "") 
              (setf colour :blue))

            (grid (make-instance 'label
                                 :text (format nil "In colour ~a" colour)
                                 :foreground colour)
                  0 0)))
