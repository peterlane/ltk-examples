(require 'asdf)
(require 'ltk)
(use-package :ltk)

(with-ltk ()
          (wm-title *tk* "scrollbar-example.lisp")
          (let ((listbox (make-instance 'listbox :height 5))
                (scrollbar (make-instance 'scrollbar :orientation :vertical))
                (status (make-instance 'label :text "Status message here")))
            (grid listbox 0 0 :sticky "nwes")
            (grid scrollbar 0 1 :sticky "ns")
            (grid status 1 0 :columnspan 2 :sticky "we")

            ; configure the scrollbar and listbox to talk to each other
            (configure scrollbar 
                       :command (format nil "~a yview" (widget-path listbox)))
            (configure listbox 
                       :yscrollcommand (format nil "~a set" (widget-path scrollbar)))
                                                  
            (grid-columnconfigure *tk* 0 :weight 1)
            (grid-rowconfigure *tk* 0 :weight 1)

            (dotimes (i 100)
              (listbox-append listbox (format nil "Line ~a of 100" (+ 1 i))))))

