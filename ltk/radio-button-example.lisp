(require 'asdf)
(require 'ltk)
(use-package :ltk)

(with-ltk ()
          (wm-title *tk* "radio-button-example.lisp")
          (let* ((button-1 (make-instance 'radio-button :text "Red" 
                                          :value :red
                                          :variable "colours"))
                 (button-2 (make-instance 'radio-button :text "Green" 
                                          :value :green
                                          :variable "colours"))
                 (button-3 (make-instance 'radio-button :text "Blue" 
                                          :value :blue
                                          :variable "colours"))
                 (show (make-instance 'button :text "Show state"
                                      :command (lambda ()
                                                 (format t "Colour: ~a~&"
                                                         (value button-3))))))

            (setf (value button-3) :blue)

            (grid button-1 0 0 :padx 5 :pady 5)
            (grid button-2 0 1 :padx 5 :pady 5)
            (grid button-3 0 3 :padx 5 :pady 5)
            (grid show 1 0 :columnspan 3)))
