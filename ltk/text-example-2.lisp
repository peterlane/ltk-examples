(require 'asdf)
(require 'ltk)
(use-package :ltk)

(defun write-to-log (msg log-text)
  "Adds 'msg' to given 'log-text', but ensures only 24-lines are shown"
  (let ((num-lines (length (uiop:split-string (text log-text) 
                                              :separator (string #\newline)))))
  (configure log-text :state :normal)
  (when (= num-lines 24)
    (format-wish "~a delete 1.0 2.0" (widget-path log-text)))
  (append-text log-text msg)
  (append-newline log-text)
  (configure log-text :state :disabled)))

(with-ltk 
  ()
  (wm-title *tk* "text-example-2.lisp")
  (let ((log-text (make-instance 'text :height 24 :width 80 
                                 :wrap :none :state :disabled)))
    (grid log-text 0 0 :sticky "nsew")       
    (grid-columnconfigure *tk* 0 :weight 1)
    (grid-rowconfigure *tk* 0 :weight 1)

    (labels ((write-msgs (n)
                         (unless (> n 100)
                           (after 100
                                  #'(lambda ()
                                      (write-to-log 
                                        (format nil "Log message ~a of 100" n)
                                        log-text)
                                      (write-msgs (+ 1 n)))))))
      (write-msgs 1))))

