(require 'asdf)
(require 'ltk)
(use-package :ltk)

(with-ltk ()
          (wm-title *tk* "text-example-1.lisp")
          (let* ((text-1 (make-instance 'scrolled-text :width 30 :height 20))
                 (onlisp "\"Lisp is worth learning for the profound enlightenment experience you will have when you finally get it; that experience will make you a better programmer for the rest of your days, even if you never actually use Lisp itself a lot.\" - Eric Raymond, \"How to Become a Hacker\"")
                 (text-2 (make-instance 'scrolled-text :width 30 :height 20)))

            (grid-columnconfigure *tk* 0 :weight 1)
            (grid-rowconfigure *tk* 0 :weight 1)

            (configure (textbox text-1) :wrap :word)
            (dotimes (i 10)
              (append-text (textbox text-1) onlisp)
              (append-newline (textbox text-1)))

            (setf (text text-2) onlisp)
            (configure (textbox text-2) :state :disabled)

            (grid text-1 0 0 :sticky "news")
            (grid text-2 0 1 :sticky "news")
            ))

