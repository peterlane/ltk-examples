(require 'asdf)
(require 'ltk)
(use-package :ltk)

(with-ltk ()
          (let ((label (make-instance 'label :text "Starting...")))
            (grid label 0 0 :padx 10 :pady 10)
            (bind label "<Enter>"
                  (lambda (evt) (setf (text label) "Moved mouse inside")))
            (bind label "<Leave>"
                  (lambda (evt) (setf (text label) "Moved mouse outside")))
            (bind label "<ButtonPress-1>"
                  (lambda (evt) (setf (text label) "Clicked left mouse button")))
            (bind label "<3>"
                  (lambda (evt) (setf (text label) "Clicked right mouse button")))
            (bind label "<Double-1>"
                  (lambda (evt) (setf (text label) "Double clicked")))
            (bind label "<B3-Motion>"
                  (lambda (evt) (setf (text label) 
                                      (format nil "Right button drag to ~a ~a"
                                              (event-x evt) (event-y evt)))))))

