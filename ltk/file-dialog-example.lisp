(require 'asdf)
(require 'ltk)
(use-package :ltk)

(with-ltk ()
          (let ((file-to-open (get-open-file :filetypes '(("Images" "*.gif") ("Lisp" "*.lisp"))
                                             :title "Open some file"))
                (files-to-save (get-save-file ))
                (directory-choice (choose-directory)))

            (format t "You chose ~&~a file to open~&~a filename for saving~&~a directory~&"
                    file-to-open files-to-save directory-choice)))
