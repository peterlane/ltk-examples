(require 'asdf)
(require 'ltk)
(use-package :ltk)

(with-ltk ()
          (wm-title *tk* "button-example.lisp")
          (let* ((button-1 (make-instance 'button :text "Simple text label"
                                         :command (lambda () (format t "button-1~&"))))
                 (image (image-load (make-image) "tcllogo.gif"))
                 (button-2 (make-instance 'button :image image
                                          :command (lambda () (format t "button-2~&"))))
                 (button-3 (make-instance 'button :image image :text "Tcl Logo"
                                          :command (lambda () (format t "button-3~&")))))

            (configure button-1 :state :disabled)
            (configure button-3 :compound :bottom)

            (bind *tk* "<Return>" (lambda (evt) (funcall (command button-2))))

            (grid button-1 0 0 :padx 5 :pady 5)
            (grid button-2 0 1 :padx 5 :pady 5)
            (grid button-3 0 3 :padx 5 :pady 5)))
