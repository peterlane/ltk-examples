(require 'asdf)
(require 'ltk)
(use-package :ltk)

(with-ltk ()
          (let* ((entry-1 (make-instance 'entry))
                 (entry-2 (make-instance 'entry :show "*"))
                 (show (make-instance 'button 
                                      :text "Show entries"
                                      :command (lambda ()
                                                 (format t "~a ~a~&"
                                                         (text entry-1)
                                                         (text entry-2))))))

            (grid (make-instance 'label :text "Name:") 0 0)
            (grid entry-1 0 1 :pady 5)
            (grid (make-instance 'label :text "Password:") 1 0)
            (grid entry-2 1 1 :pady 5)

            (grid show 2 0 :columnspan 2)))
